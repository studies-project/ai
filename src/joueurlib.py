#!/usr/bin/python
# coding: utf8


import interfacelib as ilib
import numpy as np
import random
import time


class Joueur:
	def __init__(self, partie, couleur, opts={}):
		self.couleur = couleur
		self.jeu = partie
		self.opts = opts

	def demande_coup(self):
		pass


class Humain(Joueur):

	def demande_coup(self):
		pass


class IA(Joueur):

	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie,couleur,opts)
		self.temps_exe = 0
		self.nb_appels_jouer = 0
		


class Random(IA):
	
	def demande_coup(self):
		# on appelle la fonction couleur_to_couleurval pour adapter en fonction de si l'IA est noire ou blanche.
		array=self.jeu.plateau.liste_coups_valides(ilib.couleur_to_couleurval(self.couleur))
		
		# Si on a pas de coup valide on donne coup []
		if len(array)==0 :
			return []
		
		# On utilise la fonction random.choice pour retourner un élément 
		return random.choice(array)
		

class Minmax(IA):

	def __init__(self, partie, couleur, opts={"profondeur": 6}):
		super().__init__(partie, couleur, opts=opts)
		if "profondeur" in opts:
			self.profondeur=opts["profondeur"]
			self.nombreJouer=0
		else:
			self.profondeur=5
			self.nombreJouer=0

	def evaluation(self, p, couleur):
		# On doit faire une liste des coups possibles pour évaluer chacun d'entre eux. On donne la couleur à évaluation.
		return self.jeu.plateau.scorePlateau(p, couleur)

	def minmax(self, plateau, count, couleur):
		## Quand on arrive à notre profondeur maximale
		if (count > self.profondeur):
			return (self.evaluation(plateau, couleur), None)

		# On avance sinon
		listeCoup = plateau.liste_coups_valides(couleur)

		# Fin du jeu, pas la meilleure façon de faire car on pourrait vérifier fin de partie
		if (len(listeCoup) == 0):
			return (self.evaluation(plateau, couleur), None)

		# On met à None nos coups en initialisation
		meilleurCoup = None
		pireCoup = None
		
		# On peut aussi décider du joueur avec la parité de profondeur
		if (couleur == ilib.couleur_to_couleurval(self.couleur)):
			M = -np.infty
			# Puis on passe par chaque coup de notre tableau
			for coup in listeCoup:
				temp = plateau.copie()
				temp.jouer(coup, -couleur)
				self.nombreJouer += 1

				# On prend le premier champ de la valeur retournée par minmax
				valeur = self.minmax(temp, count+1, (-1 * couleur))[0]
				if (M < valeur):
					M = valeur
					meilleurCoup = coup
			# On retourne le meilleurCoup
			return (M, meilleurCoup)

		# Recherche du pire coup pour joueur ennemi
		elif (couleur == -1 * ilib.couleur_to_couleurval(self.couleur)):
			m = np.infty
			for coup in listeCoup:
				temp = plateau.copie()
				temp.jouer(coup, -couleur)
				self.nombreJouer += 1
				
				# On prend le premier champ de la valeur retournée par minmax
				valeur = self.minmax(temp, count+1, (-1 * couleur))[0]
				if (m > valeur):
					m = valeur
					pireCoup = coup
			# On retourne le pireCoup
			return (m, pireCoup)

	def demande_coup(self):
		# On utilise la bibliothèque time pour mesurer le début et la fin d'exécution de la fonction minmax.
		start = time.time()
		# self.minmax va retourner le best coup
		resMinMax =  self.minmax(self.jeu.plateau.copie(), 1, ilib.couleur_to_couleurval(self.couleur))
		end = time.time()
		# Pour afficher le nombre de jouer et le temps, on ne l'affiche pas ici pour éviter de perdre du temps
		print("NombreJouer = ", self.nombreJouer)
		print("Durée de fonction : ", end-start)
		self.nombreJouer = 0
		# Si on a aucun coup disponible on saute le tour
		if (resMinMax[1])==None :
			return []
		return resMinMax[1]


class AlphaBeta(IA):

	def __init__(self, partie, couleur, opts={"profondeur": 8}):
		super().__init__(partie, couleur, opts=opts)
		if "profondeur" in opts:
			self.profondeur=opts["profondeur"]
			self.nombreJouer=0
		else:
			self.profondeur=5
			self.nombreJouer=0

	def evaluation(self, p, couleur):
		# On doit faire une liste des coups possibles pour évaluer chacun d'entre eux. On donne la couleur à évaluation.
		return self.jeu.plateau.scorePlateau(p, couleur)

	def alphabeta(self, plateau, count, couleur, alpha, beta):
		## Quand on arrive à notre profondeur maximale
		if (count > self.profondeur):
			return (self.evaluation(plateau, couleur), None)

		## On avance sinon
		listeCoup = plateau.liste_coups_valides(couleur)

		# Fin du jeu, pas la meilleure façon de faire car on pourrait vérifier fin de partie
		if (len(listeCoup) == 0):
			return (self.evaluation(plateau, couleur), None)

		# On met à None nos coups en initialisation
		meilleurCoup = None
		pireCoup = None
		
		# On peut aussi décider du joueur avec la parité de profondeur
		if (couleur == ilib.couleur_to_couleurval(self.couleur)):
			M = -np.infty
			# Puis on passe par chaque coup de notre tableau
			for coup in listeCoup:
				temp = plateau.copie()
				temp.jouer(coup, -couleur)
				self.nombreJouer += 1

				# On prend le premier champ de la valeur retournée par alphabeta
				valeur = self.alphabeta(temp, count+1, (-1 * couleur), alpha, beta)[0]
				if (M < valeur):
					M = valeur
					meilleurCoup = coup
					alpha = max(alpha, M)
				if (alpha >= beta):
					break
			# On retourne le meilleurCoup
			return (M, meilleurCoup)

		# Recherche du pire coup pour joueur ennemi
		elif (couleur == -1 * ilib.couleur_to_couleurval(self.couleur), alpha, beta):
			m = np.infty
			for coup in listeCoup:
				temp = plateau.copie()
				temp.jouer(coup, -couleur)
				self.nombreJouer += 1
				
				# On prend le premier champ de la valeur retournée par alphabeta
				valeur = self.alphabeta(temp, count+1, (-1 * couleur), alpha, beta)[0]
				if (m > valeur):
					m = valeur
					pireCoup = coup
					beta = min(beta, m)
				if (alpha >= beta):
					break
			# On retourne le pireCoup
			return (m, pireCoup)

	def demande_coup(self):
		start = time.time()
		# self.alphabeta va retourner le best coup
		resAlphaBeta =  self.alphabeta(self.jeu.plateau.copie(), 1, ilib.couleur_to_couleurval(self.couleur), -np.infty, np.infty)
		end = time.time()
		# Pour afficher le nombre de jouer et le temps, on ne l'affiche pas ici pour éviter de perdre du temps
		print("NombreJouer = ", self.nombreJouer)
		print("Durée de fonction : ", end-start)
		self.nombreJouer = 0
		# Si on a aucun coup disponible on saute le tour
		if (resAlphaBeta[1])==None :
			return []
		return resAlphaBeta[1]