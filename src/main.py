import jeulib as jeu
import interfacelib as ilib
import time

def launchGame(blackWin, whiteWin):
	# Vu qu'on donne à localJeu l'argument otps on doit préciser la taille
	localJeu = jeu.Jeu(8, {"choix_joueurs" : [3, 4]})
	temp = localJeu.demaNoInt()
	# Si noir a gagné
	if (temp == 1):
		blackWin += 1
	# Si blanc a gagné
	elif (temp == -1):
		whiteWin += 1
	# On utilise deux if car on prend bien en compte s'il y a égalité.
	return blackWin, whiteWin

blackWin = 0
whiteWin = 0
timeAve = 0

numberPlay = 100

# for i in range(numberPlay):
start = time.time()
blackWin, whiteWin = launchGame(blackWin, whiteWin)
end = time.time()
timeAve += end-start
	# print(timeAve)

# timeAve /= numberPlay

print("Noir = ", blackWin)
print("Blanc = ", whiteWin)
print("Temps moyen = ", timeAve)