# AI

## Description

Ce projet est un projet de Python pour la troisième année de licence.

## Usage

```sh
./run.sh
```

## Avancées

- Après avoir codé l'IA Random j'ai eu des difficultés à comprendre et à implémenter les questions suivantes. Je suis donc en train de chercher un moyen de calculer le coût d'un coup pour l'ajouter dans ma fonction d'évaluation.
- Après avoir implémenté l'algorithme minmax pour notre IA je suis en train d'implémenter le compteur et le timeur.
- Le compteur et le timeur implémenté on peut remarquer à quel point le programme prend de temps plus on avance dans le jeu. Si le premier coup lançait 363 la fonction jouer et durait 0.51 secondes, le deuxième 470 pour 0.76s, on remarque que le troisième coup lançait 32425 fois la fonction jouer et prenait presque 20 secondes pour évaluer tous les coups possibles. Plus on a de coups possibles et bien évidemment plus le code est long.
```bash
-> ./run.sh

Quel type de joueur souhaitez vous sélectionner pour noir ? Les possibilités sont :
1  : <class 'joueurlib.Humain'>
2  : <class 'joueurlib.Random'>
3  : <class 'joueurlib.Minmax'>
Tapez le nombre correspondant.
1
Quel type de joueur souhaitez vous sélectionner pour blanc ?
Les possibilités sont :
1  : <class 'joueurlib.Humain'>
2  : <class 'joueurlib.Random'>
3  : <class 'joueurlib.Minmax'>
Tapez le nombre correspondant.
3
NombreJouer =  363
Durée de fonction :  0.5098462104797363
NombreJouer =  470
Durée de fonction :  0.7630927562713623
NombreJouer =  32425
Durée de fonction :  19.363813161849976
NombreJouer =  56764
Durée de fonction :  28.51219654083252
```
- En implémentant une première version d'alpha beta les temps sont dix fois plus courts comparé au minmax, étant donné qu'on appelle bien moins de fois jouer. Ce qui permet de donner des temps si rapides.
```bash
-> ./run.sh

Quel type de joueur souhaitez vous sélectionner pour noir ? Les possibilités sont :
1  : <class 'joueurlib.Humain'>
2  : <class 'joueurlib.Random'>
3  : <class 'joueurlib.Minmax'>
4  : <class 'joueurlib.AlphaBeta'>
Tapez le nombre correspondant.
1
Quel type de joueur souhaitez vous sélectionner pour blanc ?
Les possibilités sont :
1  : <class 'joueurlib.Humain'>
2  : <class 'joueurlib.Random'>
3  : <class 'joueurlib.Minmax'>
4  : <class 'joueurlib.AlphaBeta'>
Tapez le nombre correspondant.
4
NombreJouer =  71
Durée de fonction :  0.15993690490722656
NombreJouer =  100
Durée de fonction :  0.23792266845703125
NombreJouer =  1567
Durée de fonction :  1.6396408081054688
NombreJouer =  2046
Durée de fonction :  1.6381845474243164
```
- Rajout dans le constructeur de Jeu pour gérer les options et pouvoir directement préciser à la construction du jeu quelle IA on veut lancer. Calcul de temps moyen et de nombre de victoires par joueurs en profondeur 5.
```bash
-> ./run.sh Random vs Random
Victoire de noir, avec 36 points contre 28.
Victoire de noir, avec 41 points contre 23.
[...]
Victoire de noir, avec 61 points contre 1.
Victoire de noir, avec 42 points contre 22.
Noir =  45
Blanc =  52
Temps moyen =  0.501101553440094
```
```bash
-> ./run.sh Minmax vs Minmax
Victoire de blanc, avec 50 points contre 14.
Victoire de blanc, avec 50 points contre 14.
[...]
Victoire de blanc, avec 50 points contre 14.
Victoire de blanc, avec 50 points contre 14.
Noir =  0
Blanc =  100
Temps moyen =  151.04682580471038
```
```bash
-> ./run.sh AlphaBeta vs AlphaBeta
Victoire de blanc, avec 50 points contre 14.
Victoire de blanc, avec 50 points contre 14.
[...]
Victoire de blanc, avec 50 points contre 14.
Victoire de blanc, avec 50 points contre 14.
Noir =  0
Blanc =  100
Temps moyen =  18.83702365398407
```
```bash
-> ./run.sh Random vs Minmax
Victoire de blanc, avec 36 points contre 28.
Victoire de noir, avec 38 points contre 26.
[...]
Victoire de noir, avec 59 points contre 5.
Victoire de blanc, avec 46 points contre 17.
Noir =  43
Blanc =  55
Temps moyen =  168.2870950961113
```
```bash
-> ./run.sh Random vs AlphaBeta
Victoire de noir, avec 37 points contre 27.
Victoire de noir, avec 40 points contre 24.
[...]
Victoire de noir, avec 37 points contre 27.
Victoire de noir, avec 43 points contre 21.
Noir =  43
Blanc =  55
Temps moyen =  21.56007914543152
```
```bash
-> ./run.sh Minmax vs AlphaBeta
Victoire de blanc, avec 50 points contre 14.
Victoire de blanc, avec 50 points contre 14.
[...]
Victoire de blanc, avec 50 points contre 14.
Victoire de blanc, avec 50 points contre 14.
Noir =  0
Blanc =  100
Temps moyen =  88.74599782705307
```

On remarque bien que les performances d'AlphaBeta sont bien supérieures à celle de MinMax, le nombre de victoire ne diffère pas mais le nombre d'appel aux fonctions récursives est considérablement réduit.

Pour comparer précisément j'ai décidé de mesurer le temps d'un combat entre Random et MinMax pour différentes profondeurs :
```bash
Profondeur 1:
Random x Minmax
Victoire de blanc, avec 38 points contre 26.
Temps moyen =  1.9959111213684082

Profondeur 2:
Random x Minmax
Victoire de blanc, avec 37 points contre 27.
Temps moyen =  1.549534559249878

Profondeur 3:
Random x Minmax
Victoire de noir, avec 52 points contre 12.
Temps moyen =  4.48666787147522

Profondeur 4:
Random x Minmax
Victoire de noir, avec 41 points contre 23.
Temps moyen =  32.51507544517517

Profondeur 5:
Random x Minmax
Victoire de noir, avec 42 points contre 22.
Temps moyen =  58.2508327960968

Profondeur 6:
Random x Minmax
Victoire de noir, avec 39 points contre 25.
Temps moyen =  3484.5581204891205
```
Ayant presque une heure pour une profondeur 6 on se rend bien compte que l'algorithme Minmax est trop simpliste pour cette utilisation. En utilisant cette fois ci l'algorithme AlphaBeta à partir d'une profondeur 5 les temps et nombres de jouer sont terriblements distincts.
```bash
Profondeur 5:
Random x AlphaBeta
Victoire de noir, avec 42 points contre 22.
Temps moyen =  11.503717184066772

Profondeur 6:
Random x AlphaBeta
Victoire de noir, avec 52 points contre 12.
Temps moyen =  51.26754546165466

Profondeur 7:
Random x AlphaBeta
Victoire de noir, avec 47 points contre 17.
Temps moyen =  113.6132664680481

Profondeur 8:
Random x AlphaBeta
Victoire de blanc, avec 35 points contre 29.
Temps moyen =  535.0269708633423
```
Ce qui est étonnant est que plus la profondeur augmente et plus notre IA Random semble gagner. Peut-être que le coup choisit par MinMax ne sera pas forcément le meilleur, étant donné que Random  ne fonctionne sous aucune logique ? On retrouve également cela avec notre AlphaBeta. Le temps de chaque programme peut-être ralenti de quelques microsecondes à cause de l'affichage du texte. De plus, ces estimations de victoires contre une IA Random est assez complexe de part sa nature aléatoire, on devrait créer une "IA" qui ne ferait qu'une séquence de coup identique, choisie par le développeur et ainsi s'assurer de l'efficacité et du chemin des différentes IA.

Ce qui est intéressant est de voir qu'avec une profondeur 1 dans un défi entre IA MinMax, cette dernière va très rarement dépasser pour lancer jouer la dizaine de fois :
```bash
NombreJouer =  4
Durée de fonction :  0.0053865909576416016
NombreJouer =  3
Durée de fonction :  0.004366636276245117
NombreJouer =  4
Durée de fonction :  0.0056917667388916016
NombreJouer =  6
Durée de fonction :  0.004338502883911133
NombreJouer =  4
Durée de fonction :  0.004830837249755859
NombreJouer =  7
Durée de fonction :  0.0052661895751953125
NombreJouer =  3
Durée de fonction :  0.004637956619262695
NombreJouer =  5
Durée de fonction :  0.004090785980224609
NombreJouer =  4
Durée de fonction :  0.0046787261962890625
NombreJouer =  5
Durée de fonction :  0.004746198654174805
NombreJouer =  8
Durée de fonction :  0.005129814147949219
NombreJouer =  8
Durée de fonction :  0.004150867462158203
NombreJouer =  7
Durée de fonction :  0.004883766174316406
NombreJouer =  10
Durée de fonction :  0.005341053009033203
NombreJouer =  6
Durée de fonction :  0.004446744918823242
NombreJouer =  7
Durée de fonction :  0.004461765289306641
NombreJouer =  6
Durée de fonction :  0.0035026073455810547
NombreJouer =  8
Durée de fonction :  0.004462242126464844
NombreJouer =  4
Durée de fonction :  0.003930330276489258
NombreJouer =  9
Durée de fonction :  0.0043833255767822266
NombreJouer =  2
Durée de fonction :  0.0028934478759765625
NombreJouer =  9
Durée de fonction :  0.004289150238037109
NombreJouer =  0
Durée de fonction :  0.0031075477600097656
NombreJouer =  10
Durée de fonction :  0.009042501449584961
NombreJouer =  0
Durée de fonction :  0.0030226707458496094
NombreJouer =  9
Durée de fonction :  0.004080772399902344
NombreJouer =  1
Durée de fonction :  0.003085613250732422
NombreJouer =  10
Durée de fonction :  0.0042264461517333984
NombreJouer =  1
Durée de fonction :  0.002440214157104492
NombreJouer =  9
Durée de fonction :  0.0038318634033203125
NombreJouer =  0
Durée de fonction :  0.002633810043334961
NombreJouer =  8
Durée de fonction :  0.0028853416442871094
NombreJouer =  1
Durée de fonction :  0.0026819705963134766
NombreJouer =  7
Durée de fonction :  0.0033440589904785156
NombreJouer =  2
Durée de fonction :  0.0027031898498535156
NombreJouer =  8
Durée de fonction :  0.0032737255096435547
NombreJouer =  2
Durée de fonction :  0.0024924278259277344
NombreJouer =  8
Durée de fonction :  0.002773761749267578
NombreJouer =  3
Durée de fonction :  0.0025072097778320312
NombreJouer =  10
Durée de fonction :  0.0033028125762939453
NombreJouer =  7
Durée de fonction :  0.0026938915252685547
NombreJouer =  5
Durée de fonction :  0.002489328384399414
NombreJouer =  6
Durée de fonction :  0.0019745826721191406
NombreJouer =  7
Durée de fonction :  0.002763509750366211
NombreJouer =  7
Durée de fonction :  0.0019352436065673828
NombreJouer =  3
Durée de fonction :  0.002033233642578125
NombreJouer =  8
Durée de fonction :  0.002424955368041992
NombreJouer =  3
Durée de fonction :  0.0015208721160888672
NombreJouer =  8
Durée de fonction :  0.0022516250610351562
NombreJouer =  7
Durée de fonction :  0.002130746841430664
NombreJouer =  5
Durée de fonction :  0.002566814422607422
NombreJouer =  6
Durée de fonction :  0.0014772415161132812
NombreJouer =  5
Durée de fonction :  0.0017201900482177734
NombreJouer =  7
Durée de fonction :  0.001928567886352539
NombreJouer =  6
Durée de fonction :  0.00174713134765625
NombreJouer =  5
Durée de fonction :  0.001489400863647461
NombreJouer =  4
Durée de fonction :  0.0009560585021972656
NombreJouer =  5
Durée de fonction :  0.0015385150909423828
NombreJouer =  2
Durée de fonction :  0.000762939453125
NombreJouer =  3
Durée de fonction :  0.0006148815155029297
NombreJouer =  3
Durée de fonction :  0.000720977783203125
NombreJouer =  1
Durée de fonction :  0.0004947185516357422
NombreJouer =  1
Durée de fonction :  0.0002911090850830078
NombreJouer =  0
Durée de fonction :  0.00012111663818359375
NombreJouer =  0
Durée de fonction :  0.00011110305786132812
Victoire de blanc, avec 39 points contre 25.
Temps moyen =  0.9415404796600342
```
Ce temps est étonnement plus court que le temps donné par un duel entre AlphaBeta de même profondeur, mais garde des statistiques ressemblantes :
```bash
NombreJouer =  4
Durée de fonction :  0.005666017532348633
NombreJouer =  3
Durée de fonction :  0.00418400764465332
NombreJouer =  4
Durée de fonction :  0.005354166030883789
NombreJouer =  6
Durée de fonction :  0.009200811386108398
NombreJouer =  4
Durée de fonction :  0.00520014762878418
NombreJouer =  7
Durée de fonction :  0.005353450775146484
NombreJouer =  3
Durée de fonction :  0.0048367977142333984
NombreJouer =  5
Durée de fonction :  0.005079984664916992
NombreJouer =  4
Durée de fonction :  0.004757881164550781
NombreJouer =  5
Durée de fonction :  0.003965616226196289
NombreJouer =  8
Durée de fonction :  0.0051538944244384766
NombreJouer =  8
Durée de fonction :  0.004136800765991211
NombreJouer =  7
Durée de fonction :  0.005000114440917969
NombreJouer =  10
Durée de fonction :  0.00419926643371582
NombreJouer =  6
Durée de fonction :  0.0045969486236572266
NombreJouer =  7
Durée de fonction :  0.004678964614868164
NombreJouer =  6
Durée de fonction :  0.0036568641662597656
NombreJouer =  8
Durée de fonction :  0.004581451416015625
NombreJouer =  4
Durée de fonction :  0.0031211376190185547
NombreJouer =  9
Durée de fonction :  0.004572629928588867
NombreJouer =  2
Durée de fonction :  0.0036275386810302734
NombreJouer =  9
Durée de fonction :  0.004456281661987305
NombreJouer =  0
Durée de fonction :  0.003319263458251953
NombreJouer =  10
Durée de fonction :  0.0034635066986083984
NombreJouer =  0
Durée de fonction :  0.003200531005859375
NombreJouer =  9
Durée de fonction :  0.003400564193725586
NombreJouer =  1
Durée de fonction :  0.003192901611328125
NombreJouer =  10
Durée de fonction :  0.0037126541137695312
NombreJouer =  1
Durée de fonction :  0.005627632141113281
NombreJouer =  9
Durée de fonction :  0.0033750534057617188
NombreJouer =  0
Durée de fonction :  0.0028047561645507812
NombreJouer =  8
Durée de fonction :  0.0037605762481689453
NombreJouer =  1
Durée de fonction :  0.0021131038665771484
NombreJouer =  7
Durée de fonction :  0.006318092346191406
NombreJouer =  2
Durée de fonction :  0.0039081573486328125
NombreJouer =  8
Durée de fonction :  0.0036156177520751953
NombreJouer =  2
Durée de fonction :  0.0027458667755126953
NombreJouer =  8
Durée de fonction :  0.00360107421875
NombreJouer =  3
Durée de fonction :  0.003083944320678711
NombreJouer =  10
Durée de fonction :  0.0029480457305908203
NombreJouer =  7
Durée de fonction :  0.002354860305786133
NombreJouer =  5
Durée de fonction :  0.0020978450775146484
NombreJouer =  6
Durée de fonction :  0.0032727718353271484
NombreJouer =  7
Durée de fonction :  0.002275228500366211
NombreJouer =  7
Durée de fonction :  0.0026154518127441406
NombreJouer =  3
Durée de fonction :  0.0021505355834960938
NombreJouer =  8
Durée de fonction :  0.002570629119873047
NombreJouer =  3
Durée de fonction :  0.001943826675415039
NombreJouer =  8
Durée de fonction :  0.002301454544067383
NombreJouer =  7
Durée de fonction :  0.003820657730102539
NombreJouer =  5
Durée de fonction :  0.002252340316772461
NombreJouer =  6
Durée de fonction :  0.0019443035125732422
NombreJouer =  5
Durée de fonction :  0.0014362335205078125
NombreJouer =  7
Durée de fonction :  0.004043102264404297
NombreJouer =  6
Durée de fonction :  0.003793478012084961
NombreJouer =  5
Durée de fonction :  0.00456690788269043
NombreJouer =  4
Durée de fonction :  0.001230478286743164
NombreJouer =  5
Durée de fonction :  0.0028972625732421875
NombreJouer =  2
Durée de fonction :  0.0009520053863525391
NombreJouer =  3
Durée de fonction :  0.000720977783203125
NombreJouer =  3
Durée de fonction :  0.0008258819580078125
NombreJouer =  1
Durée de fonction :  0.0005252361297607422
NombreJouer =  1
Durée de fonction :  0.0006835460662841797
NombreJouer =  0
Durée de fonction :  7.581710815429688e-05
NombreJouer =  0
Durée de fonction :  8.916854858398438e-05
Victoire de blanc, avec 39 points contre 25.
Temps moyen =  1.0815038681030273
```
## Difficultés

Je voulais pouvoir lancer le code sur un docker pour pouvoir l'exécuter à distance, malheureusement la dépendances des fenêtres empêche de pouvoir lancer le programme sans devoir changer le code pour retirer toutes les options graphiques. Ne voulant pas casser la comptabilité avec la GUI j'ai changé d'idée.

Pour l'amélioration du AlphaBeta je n'arrive pas à gérer les coups de façon efficace. L'idée que j'ai serais pour chaque coup de faire une seconde boucle et de lancer la fonction de score sur chacun des coups possibles, vérifier la valeur du coup et prendre celui avec le score le plus haut si l'IA est le joueur courant et le plus bas si l'ennemi est le joueur courant. Le problème étant que la structure recursive va faire lancer cette fonction des milliers de fois et ralentir bien plus l'exécution du programme. Si j'arrivais à l'implémenter on peut estimer son déroulement :

Si nous ne sommes pas sur une feuille et qu'on ne dépasse pas notre profondeur on fait une boucle sur les coups disponibles. On vérifie leurs scores, puis en sortant de la boucle on joue le coup qui est soit avec le plus grand score soit le plus bas en fonction du joueur courant. Le plus compliqué étant de gérer cela pour éviter de lancer une récursivité sur tous nos coups et ralentir notre programme.

Pour une meilleure fonction d'évaluation on peut penser que les coins sont les points les plus intéressants car avoir les quatres coins peut totalement changer une fin de partie. Les bords ausis sont très avantageux. On peut penser qu'un coin vaut 5 points, un carré du bord 3 points et les autres carrés 1 point, le centre lui pourrait valoir 10 points. Le centre mêlé aux coins est un très bon combo pour gagner une partie. On pourrait ensuite aller plus loin et se dire que les points sur les lignes et diagonales du centre valent plus que les points standard.